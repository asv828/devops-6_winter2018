#!/bin/bash
echo "Clearing in progress ..."
ROOT_FOLDER=projects
rm -r /$ROOT_FOLDER
#
DEVELOPERS=(R1 R2 R3 R4 R5)
MANAGERS=(I1 I2 I3)
ANALITICS=(A1 A2 A3 A4)
#
for USER in "${DEVELOPERS[@]}"
do 
userdel -fr $USER
done
#
for USER in "${MANAGERS[@]}"
do
userdel -rf $USER
done
#
for USER in "${ANALITICS[@]}"
do
userdel -rf $USER
done

