#!/bin/bash
echo "Please, wait a moment, the script is working ..."
F=projects
F1=Proj1
F2=Proj2
F3=Proj3
#------------
DEV=(R1 R2 R3 R4 R5)
MAN=(I1 I2 I3)
ANL=(A1 A2 A3 A4)
#------------
F1RW=(R2 R3 R5 A1)
F1RO=(A4)
F2RW=(R1 R5 A1)
F2RO=(A2 A3)
F3RW=(R1 R2 R4 A2)
F3RO=(A1 A4)
#------------
cmd1='useradd -G sudo -p p@ssw0rd -s /bin/bash'
cmd2='setfacl -d -m'
#------------
echo "Making directory structure ..." 
#------------
mkdir /$F /$F/$F1 /$F/$F2 /$F/$F3
#------------
echo "Done!"
echo "Add users ..."
#------------
for USER in ${DEV[@]}
do 
$cmd1 $USER
done
#------------
for USER in ${MAN[@]}
do
$cmd1 $USER
done
#------------
for USER in ${ANL[@]}
do
$cmd1 $USER
done
#------------
echo "Done!"
echo "Setting ACL ..."
#------------
for USER in ${F1RW[@]}
do
$cmd2 u:$USER:rw /$F/$F1
done
#------------
for USER in ${F1RO[@]}
do
$cmd2 u:$USER:r /$F/$F1
done
#------------
for USER in ${F2RW[@]}
do
$cmd2 u:$USER:rw /$F/$F2
done
#------------
for USER in ${F2RO[@]}
do
$cmd2 u:$USER:r /$F/$F2
done
#------------
for USER in ${F3RW[@]}
do
$cmd2 u:$USER:rw /$F/$F3
done
#------------
for USER in ${F3RO[@]}
do
$cmd2 u:$USER:r /$F/$F3
done
#------------
echo "The task has done!"
